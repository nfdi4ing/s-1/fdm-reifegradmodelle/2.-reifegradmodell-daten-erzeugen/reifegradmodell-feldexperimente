# Reifegradmodell Feldexperimente


Es stehen zwei relevante Dateien zur Verfügung. 
Einmal [Übersichtsfolien](https://git.rwth-aachen.de/nfdi4ing/s-1/fdm-reifegradmodelle/2.-reifegradmodell-daten-erzeugen/reifegradmodell-feldexperimente/-/blob/main/RGM_Feldversuche.pdf) zu der Phase der Datenerzeugung in Feldexperimenten mit einleitenden Worten, zugehörigen Aktivitäten und dem entwickelten Inhalten des Reifegradmodells. Des Weiteren steht eine [Checkliste](https://git.rwth-aachen.de/nfdi4ing/s-1/fdm-reifegradmodelle/2.-reifegradmodell-daten-erzeugen/reifegradmodell-feldexperimente/-/blob/main/Checkliste_Feldversuche.docx) zur Bewertung des Reifegrades zur Verfügung. Die Checkliste lässt sich innerhalb einzelner Forschungsprojekte für die Reifegradbewertung zur Umsetzung der Datenerzeugung in Feldexperimenten einsetzen. Jeder Reifegrad dieses Modells wurde eingehend Charakterisiert und für jeden Reifegrad wurden umzustetzende Ziele definiert. Bei Erfüllung aller Ziele eines Reifegrades wird einem diese Reifestufe attestiert.  

